<?php
require 'connect.php';

// Get the posted data.
$postdata = file_get_contents("php://input");

if(isset($postdata) && !empty($postdata))
{
  // Extract the data.
  $request = json_decode($postdata);
	
  // Validate.
  if ((int)$request->data->id < 1 || trim($request->data->object) == '' || trim($request->data->catagorie) == '' || (int)$request->data->price < 1 || trim($request->data->merk) == '') {
    return http_response_code(400);
  }
    
  // Sanitize.
  $id    = mysqli_real_escape_string($con, (int)$request->data->id);
  $object = mysqli_real_escape_string($con, trim($request->data->object));
  $catagorie = mysqli_real_escape_string($con, trim($request->data->catagorie));
  $price = mysqli_real_escape_string($con, (int)$request->data->price);
  $merk = mysqli_real_escape_string($con, trim($request->data->merk));


  // Update.
  $sql = "UPDATE `onderdelen` SET `object`='$object',`catagorie`='$catagorie',`price`='$price', `merk`='$merk' WHERE `id` = '{$id}' LIMIT 1";

  if(mysqli_query($con, $sql))
  {
    http_response_code(204);
  }
  else
  {
    return http_response_code(422);
  }  
}
