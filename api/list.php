<?php
/**
 * Returns the list of cars.
 */
require 'connect.php';
    
$onderdelen = [];
$sql = "SELECT id, object, catagorie, price, merk FROM onderdelen";

if($result = mysqli_query($con,$sql))
{
  $od = 0;
  while($row = mysqli_fetch_assoc($result))
  {
    $onderdelen[$od]['id']    = $row['id'];
    $onderdelen[$od]['object'] = $row['object'];
    $onderdelen[$od]['catagorie'] = $row['catagorie'];
    $onderdelen[$od]['price'] = $row['price'];
    $onderdelen[$od]['merk'] = $row['merk'];
    $od++;
  }
    
  echo json_encode(['data'=>$onderdelen]);
}
else
{
  http_response_code(404);
}
