<?php
require 'connect.php';

// Get the posted data.
$postdata = file_get_contents("php://input");

if(isset($postdata) && !empty($postdata))
{
  // Extract the data.
  $request = json_decode($postdata);
	

  // Validate.
  if( trim($request->data->object) == '' || trim($request->data->catagorie) == '' || (int)$request->data->price < 1 || trim($request->data->merk) == '')
  {
    return http_response_code(400);
  }
	
  // Sanitize.
  $object = mysqli_real_escape_string($con, trim($request->data->object));
  $catagorie = mysqli_real_escape_string($con, trim($request->data->catagorie));
  $price = mysqli_real_escape_string($con, (int)$request->data->price);
  $merk = mysqli_real_escape_string($con, trim($request->data->merk));
  
    

  // Store.
  $sql = "INSERT INTO `onderdelen`(`id`,`object`,`catagorie`,`price`,`merk`) VALUES (null,'{$object}','{$catagorie}','{$price}','{$merk}')";

  if(mysqli_query($con,$sql))
  {
    http_response_code(201);
    $onderdelen = [
      'object' => $object,
      'catagorie' => $catagorie,
      'price' => $price,
      'merk' => $merk,
      'id'    => mysqli_insert_id($con)
    ];
    echo json_encode(['data'=>$onderdelen]);
  }
  else
  {
    http_response_code(422);
  }
}
