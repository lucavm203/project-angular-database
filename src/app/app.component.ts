import { Component, OnInit } from '@angular/core';
import { NgForm } from '@angular/forms';

import { Onderdelen } from './onderdelen';
import { OnderdelenService } from './onderdelen.service';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css'],
})
export class AppComponent implements OnInit {
  onderdelen: Onderdelen[] = [];
  onderdeel: Onderdelen = { object: '',catagorie: '', price: 0 , merk: ''};

  error = '';
  success = '';

  constructor(private onderdelenService: OnderdelenService) {}

  ngOnInit() {
    this.getOnderdelen();
  }

  resetAlerts() {
    this.error = '';
  }

  getOnderdelen(): void {
    this.onderdelenService.getAll().subscribe(
      (data: Onderdelen[]) => {
        this.onderdelen = data;
        this.success = 'Success in retrieving the list';
      },
      (err) => {
        this.error = err.message;
      }
    );
  }

  addOnderdelen(f: NgForm) {
    this.resetAlerts();

    this.onderdelenService.store(this.onderdeel).subscribe(
      (res: Onderdelen) => {
        // Update the list of cars
        this.onderdelen.push(res);

        // Inform the user
        this.success = 'Created successfully';

        // Reset the form
        f.reset();
      },
      (err) => (this.error = err.message)
    );
  }

  updateOnderdelen(name: any, cat: any, price: any, mer: any, id: any) {
    this.resetAlerts();

    this.onderdelenService
      .update({ object: name.value, catagorie: cat.value, price: price.value, merk: mer.value, id: +id })
      .subscribe(
        (res) => {
          this.success = 'Updated successfully';
        },
        (err) => (this.error = err)
      );
  }

  deleteOnderdelen(id: number) {
    this.resetAlerts();
    this.onderdelenService.delete(id).subscribe(
      (res) => {
        this.onderdelen = this.onderdelen.filter(function (item) {
          return item['id'] && +item['id'] !== +id;
        });

        this.success = 'Deleted successfully';
      },
      (err) => (this.error = err)
    );
  }
}
