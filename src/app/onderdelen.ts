export interface Onderdelen {
  object: string;
  catagorie: string;
  price: number;
  merk: string;
  id?: number;
}
