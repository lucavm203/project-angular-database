import { Injectable } from '@angular/core';
import { HttpClient, HttpParams } from '@angular/common/http';

import { map } from 'rxjs/operators';
import { Onderdelen } from './onderdelen';

@Injectable({
  providedIn: 'root',
})
export class OnderdelenService {
  baseUrl = 'http://localhost/api/';

  constructor(private http: HttpClient) {}

  getAll() {
    return this.http.get(`${this.baseUrl}list`).pipe(
      map((res: any) => {
        return res['data'];
      })
    );
  }

  store(onderdeel: Onderdelen) {
    return this.http.post(`${this.baseUrl}/store`, { data: onderdeel }).pipe(
      map((res: any) => {
        return res['data'];
      })
    );
  }

  update(onderdeel: Onderdelen) {
    return this.http.put(`${this.baseUrl}/update`, { data: onderdeel });
  }

  delete(id: any) {
    const params = new HttpParams()
      .set('id', id.toString());

    return this.http.delete(`${this.baseUrl}/delete`, { params: params });
  }
}
